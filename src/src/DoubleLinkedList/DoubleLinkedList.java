package src.DoubleLinkedList;

import java.util.Iterator;

import src.nodo.Nodo;

public class DoubleLinkedList<T> implements Iterable<T> {

	Nodo <T> end=null,start=null;


	public DoubleLinkedList(){
	
		end = new Nodo<T>();
		start = new Nodo<T>();
		start.setIndex(-1);
		end.setIndex(-1);
	
	}
	
	
	public DoubleLinkedList(T vaue) {
	
		this();
		Nodo <T>temp = new Nodo<T>(vaue);
		temp.setIndex(0);
		start.setNext(temp);
		end.setBack(temp);
	     
	}
	
	public void addBegin(T value){
		Nodo <T> temp = new Nodo<T>(value);
		temp.setNext(start.getNext());
		start.setNext(temp);
		if(temp.getNext() != null){
			temp.getNext().setBack(temp);
		}
		else
			end.setBack(temp);
		
		reIndex();
	}
	
	public void addBegin(Nodo value){
		Nodo <T> temp = value;
		temp.setNext(start.getNext());
		start.setNext(temp);
		if(temp.getNext() != null){
			temp.getNext().setBack(temp);
		}
		else
			end.setBack(temp);
		
		reIndex();
	}
	
	
	public void addEnd(T value){
		
		Nodo <T>temp = new Nodo<T>(value);
		temp.setBack(end.getBack());
		end.setBack(temp);
		if(temp.getBack() != null){
			temp.getBack().setNext(temp);
			temp.setIndex(temp.getBack().getIndex() + 1);
		}
		else
			start.setNext(temp);
		
	}
	
	 public void  clear(){
		 Nodo<T> temp = start;
		 while (temp.getNext() != null){
			 Nodo<T> temp2 = temp.getNext();
			 temp.setNext(null);
			 temp = temp2;
			 temp.setBack(null);
		 }
		 end.setBack(null);
		 System.gc();
	 }
	 
	public void printPalante(){
		
		Nodo<T> temp = start;
		while(temp.getNext()!=null){
			
			temp = temp.getNext();
			System.out.print(temp.getValue()+" ");
		}
		System.out.println();
	
	}
	
	public void printPatras(){
		
		Nodo <T> temp = end;
		while(temp.getBack()!=null){
			temp = temp.getBack();
			System.out.print(temp.getValue()+" ");
		}
	System.out.println();
	}
	
	public int indexOf (T value){
		
		 Nodo<T> temp = ExisteElemento(value);
			 if(temp != null)
			      return (int) temp.getIndex();
			 else return -1;
			
	 }
	public Nodo<T> searchDoble(T value){
		if (start.getNext()== null && end.getBack()==null){
			return null;
		}
		Nodo <T>temp1 = start.getNext();
		Nodo <T>temp2 = end.getBack();
		int n = (int) end.getBack().getIndex();
		
		for(int i = 0 ; i<= n/2 ; i++){
			if(temp1.getValue().equals(value)){
				return temp1;
			}
			if(temp2.getValue().equals(value)){
				return temp2;
			}
			temp1 = temp1.getNext();
			temp2 = temp2.getBack();
			
		}
		return null;
		
	
	}
	
	public Nodo<T> searchDoble(Nodo value){
		if (start.getNext()== null && end.getBack()==null){
			return null;
		}
		Nodo <T>temp1 = start.getNext();
		Nodo <T>temp2 = end.getBack();
		int n = (int) end.getBack().getIndex();
		
		for(int i = 0 ; i<= n/2 ; i++){
			if(temp1==value){
				return temp1;
			}
			if(temp2==value){
				return temp2;
			}
			temp1 = temp1.getNext();
			temp2 = temp2.getBack();
			
		}
		return null;
		
	}
	
	
	
	public void removeLast(){
		Nodo <T> tmp = end.getBack();
		if(tmp!=null){
			end.setBack(tmp.getBack());
			if(tmp.getBack()!=null)
				tmp.getBack().setNext(null);
			else
				start.setNext(null);
		}
	}
	public boolean remove (T value){
		Nodo <T> temp = searchDoble(value);
		
		if (temp != null){
		
			if(temp.getNext()!=null)    
				temp.getNext().setBack(temp.getBack()); 
			else 						
				end.setBack(temp.getBack());
			
			if(temp.getBack()!=null)		
				temp.getBack().setNext(temp.getNext());
			else 						
				start.setNext(temp.getNext());	
			reIndex();
			return true;
		}
		return false;
	}
	
	public boolean remove (Nodo temp){
		
		if (temp != null){
		
			if(temp.getNext()!=null)    
				temp.getNext().setBack(temp.getBack()); 
			else 						
				end.setBack(temp.getBack());
			
			if(temp.getBack()!=null)		
				temp.getBack().setNext(temp.getNext());
			else 						
				start.setNext(temp.getNext());	
			reIndex();
			return true;
		}
		return false;
	}
	
	
	public void removeFirst(){
		Nodo <T> tmp = start.getNext();
		if(tmp!=null){
			start.setNext(tmp.getNext());
			if(tmp.getNext()!=null)
				tmp.getNext().setBack(null);
			else
				end.setBack(null);
		}
		reIndex();
	}
	
	public Nodo<T> getFirst(){
		return start.getNext();
	}
	public Nodo<T> getLast(){
		return end.getBack();
	}
	public boolean isEmpty(){
		if(start.getNext() != null)
			return false;
		
			return true;
		}
	
	
	public boolean replace(T val ,T nu){
		Nodo<T> temp  = searchDoble(val);
		 if (temp != null){
			 temp.setValue(nu);
		 return true;
		 }
	
		 return false ;
	 
	}
	public boolean addAfter(T val,T nv){
		Nodo<T> temp = searchDoble(val);
		if(temp != null){
			Nodo<T> nn = new Nodo<T>(nv);
			nn.setNext(temp.getNext());
			temp.setNext(nn);
			nn.setBack(temp);
			if(nn.getNext()!=null){
				nn.getNext().setBack(nn);
			}
			else 
				end.setBack(nn);
			return true;
		}
		return false;
	}
	
	public boolean addAfter(Nodo val,Nodo nv){
	
		if(val != null){
			Nodo<T> nn = nv;
			nn.setNext(val.getNext());
			val.setNext(nn);
			nn.setBack(val);
			if(nn.getNext()!=null){
				nn.getNext().setBack(nn);
			}
			else 
				end.setBack(nn);
			return true;
		}
		return false;
	}
	
	
	public boolean addBefore(T val,T nv){
		Nodo<T> temp = searchDoble(val);
		if(temp != null){
			Nodo<T> nn = new Nodo<T>(nv);
			nn.setBack(temp.getBack());
			temp.setBack(nn);
			nn.setNext(temp);
			if(nn.getBack()!=null){
				nn.getBack().setNext(nn);
			}
			else
				start.setNext(nn);
			return true;
		}
		return false;
	}
	public void removeAfter(T val){
		Nodo<T> temp = searchDoble(val);
		if(temp!=null)
			if(temp.getNext()!=null){
				temp.setNext(temp.getNext().getNext());
				if(temp.getNext().getNext()!=null){
					temp.getNext().getNext().setBack(temp);
					temp.setNext(temp.getNext().getNext());
				}
				else{
					end.setBack(temp);
				    temp.setNext(null);
				}
			}
	}
	
	public void removeBefore(T val){
		Nodo<T> temp = searchDoble(val);
		if(temp!=null)
			if(temp.getBack()!=null)
			{
				temp.setBack(temp.getBack().getBack());
				if(temp.getBack().getBack()!=null){
					temp.getBack().getBack().setNext(temp);
					temp.setBack(temp.getBack().getBack());
				}
				else{
					start.setNext(temp);	
					temp.setBack(null);
				}
			}
		
	}
	
	public void reIndex(){
	
		Nodo<T> temp = start;
		int cont = 0;
		while(temp.getNext()!=null){
		temp = temp.getNext();
		temp.setIndex(cont++);
		
		}
	}
	public Nodo<T> getByIndex(int i){
		 Nodo<T> temp = start;
		 int n=0;
		 while ( n != i ) {
			 if(temp.getNext() != null)
			      temp= temp.getNext();
			n++;
		 }
		 return temp;
	}
	@Override
	public Iterator<T> iterator() {
		
		return new Iterator<T>(){
			int i = 0;
		
			@Override
			public boolean hasNext() {
				
					return getByIndex(i).getNext()!=null ? true : false ;
			
			}

			@Override
			public T next() {
				
					return (T) getByIndex(++i).getValue();
				
			}
		
			
		};
 	}

	public long size(){
		reIndex();
		return (end.getBack()!=null)?end.getBack().getIndex()+1:-1;
	}

	public boolean SonIguales(DoubleLinkedList<T> lista){
		if (lista.size() == this.size()){
			return SonIguales(lista, this);
		}
		return false;
	}
	
	boolean SonIguales(DoubleLinkedList<T> lista,DoubleLinkedList<T> thisLista){
		Nodo<T> tmp = lista.start.getNext();
		Nodo<T> thisTmp = thisLista.start.getNext();
		
		if (tmp.getValue().equals(thisTmp.getValue())) return SonIguales(tmp,thisTmp);
		return false;
	}
	
	boolean SonIguales(Nodo<T> tmp, Nodo<T> thisTmp){
		if (tmp.getIndex() == thisTmp.getIndex()) return true;
		if (tmp.getValue().equals(thisTmp.getValue())) return SonIguales(tmp.getNext(),thisTmp.getNext());
		return false;
	}
	
	public Nodo<T> ExisteElemento(T temp){
	      return ExisteElemento(start,temp);
	}
	
	private Nodo<T> ExisteElemento(Nodo<T> se,T comp){
			
	   if (se.getNext()==null) return null;
		
	   if(se.getNext().getValue().equals(comp)) return se.getNext();
				
	   return 	ExisteElemento(se.getNext(),comp);			
	   
	}
	
	public int Ocurrencia(T value){
		return Ocurrencia(value,start.getNext());
	}

	int Ocurrencia(T value,Nodo<T> tmp){
		if (tmp != null){
			if (tmp.getValue().equals(value)) {
				return Ocurrencia(value, tmp.getNext()) + 1;
			}
			else return Ocurrencia(value, tmp.getNext());
		}
		return 0;
	}
	
	public int Suma(){
		return Suma(start.getNext());
	}
	
	public int Suma(Nodo<T> tmp){
		if (tmp != null){
			//if (tmp.getValue().getClass().equals("class java.lang.Integer")){
				return Suma(tmp.getNext()) + (int) tmp.getValue();
			//}		
		}
		return 0;
	}
	
	public void  merge(DoubleLinkedList<T> dll){
	Merge(start.getNext(),dll.getFirst());
	this.sort();
    }
	
	
	public void Merge(Nodo<T> my , Nodo<T> plus){
		if(my !=null && plus !=null){
			Nodo nx= plus.getNext();
			this.addAfter(my, plus);
			Merge(my.getNext().getNext(),nx);
		}
		else if(plus !=null){
			this.addEnd( plus.getValue());
			Merge(null,plus.getNext());
		}
	}

	public <T extends Comparable<T>> T Maximo(){
		if(start.getNext()!=null)
			return Maximo(start.getNext(),(T) start.getNext().getValue());
		else
			return null;
	}
	private  <T extends Comparable<T>> T Maximo (Nodo n,T maxV){
		if(n!=null){
			if(maxV.compareTo((T) n.getValue()) <= 0 ) {
				return Maximo(n.getNext(), (T) n.getValue());
			}
			return Maximo(n.getNext(),maxV);
		}
		else return maxV;
	}
	
	
	
	public <T extends Comparable<T>> Nodo MaximoN(){
		if(start.getNext()!=null)
			return MaximoN(start.getNext(), start.getNext());
		else
			return null;
	}
	private  <T extends Comparable<T>> Nodo MaximoN (Nodo n, Nodo maxV){
		if(n!=null){
			T val = (T) maxV.getValue();
			if(val.compareTo((T) n.getValue()) <= 0 ) {
				return MaximoN(n.getNext(), n);
			}
			return MaximoN(n.getNext(),maxV);
		}
		else {
			
			return maxV;
		}
	}
	
	
	public void sort(){
		sort(start.getNext());
	}
	public void  sort  (Nodo<T> n){
		Nodo<T> temp = MaximoN(n.getNext(),n.getNext());
		if(n.getNext()!=null){
			
			remove(temp);
			addBegin( temp);
			//printPalante();
			sort(n);
		
		}
		else{
			temp =getLast();
			if(temp!= MaximoN(start.getNext(),start.getNext()) ){
			remove(temp);
		    addBegin( temp);
			}
		}
		
	}
	
	
}
