package src.nodo;

public class Nodo <T> implements Comparable<T>{
	
	private T Value;
	private Nodo<T> next = null;
	private Nodo<T> back = null;
	private long index = 0;
	
		public long getIndex() {
			return index;
		}

		public void setIndex(long index) {
			this.index = index;
		}	

		public Nodo() {
			this.Value=null;
		}

		public Nodo(T v){
			this.Value = v;
		}
		
	public T getValue() {
		return Value;
	}
	public void setValue(T value) {
		Value = value;
	}
	public Nodo<T> getNext() {
		return next;
	}
	public void setNext(Nodo<T> next) {
		this.next = next;
	}
	public Nodo<T> getBack() {
		return back;
	}
	public void setBack(Nodo<T> back) {
		this.back = back;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}


	
	@Override
	public int compareTo(T o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
