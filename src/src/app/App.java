package src.app;

import src.DoubleLinkedList.DoubleLinkedList;
import src.nodo.Nodo;

public class App {
	public static void main(String[] args) {
	DoubleLinkedList<String> dll = new DoubleLinkedList<String>();
	DoubleLinkedList<String> dll2 = new DoubleLinkedList<String>();
	DoubleLinkedList<Integer> dllI = new DoubleLinkedList<Integer>();
	
	dll.addBegin("Jalil");
	dll.addBegin("Alex");
	dll.addBegin("Luis");
	dll.addBegin("Pedro");
	dll.addBegin("ZZZ");

	
	dll2.addBegin("Jalil");
	dll2.addBegin("Alex");
	dll2.addBegin("Luis");
	dll2.addBegin("Pedro");
	dll2.addBegin("Pedro");

	dllI.addBegin(4);
	dllI.addBegin(2);
	dllI.addBegin(3);
	dllI.addBegin(5);
	dllI.addBegin(1);
	
	
	
	
	//Ejemplo 1 SonIguales
	System.out.println("Son iguales? " + dll.SonIguales(dll2));
	
	//Ejemplo 2 ExisteElemento
	if (dll.ExisteElemento("Alex") != null) System.out.println("Lo encontr�"); 
	
	//Ejemplo 3 Ocurrencia
	System.out.println("Se repiti� " + dll2.Ocurrencia("Pedro") + " veces.");
	
	//Ejemplo 4 Suma
	System.out.println("Suma: " + dllI.Suma());
	
	//Ejemplo 5 Merge 
	dll.merge(dll2);
	dll.printPalante();
	
	//Ejemplo 6 Maximo
	System.out.println("M�ximo: " + dll.Maximo());
	
	
	
	}
}
